<?php

declare(strict_types=1);

namespace App\Services;

use App\Interfaces\CollectionInterface;

class NumberCollection implements CollectionInterface
{
    private array $values;

    public function __construct()
    {
        $this->values = [];
    }

    public function add(int $value): void
    {
        $this->values[$value] = isset($this->values[$value]) ? $this->values[$value] + 1 : 1;
    }

    public function optimize(): void
    {
        //optimize values to select
    }

    /**
     * @param int $start
     * @param int $end
     * @return int[]
     */
    public function select(int $start, int $end): array
    {
        $result = [];

        foreach ($this->values as $key => $value) {
            if ($key < $start || $key > $end) continue;

            if ($value > 1) {
                array_merge($result, $this->getValuesMap($key, $value));
                continue;
            }

            $result[] = $value;
        }

        return $result;
    }

    private function getValuesMap(int $number, int $count): array
    {
        $result = [];

        for ($i = 0; $i <= $count; $i++) {
            $result[] = $number;
        }

        return $result;
    }

}