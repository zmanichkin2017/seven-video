<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Services\NumberCollection;
use Illuminate\Console\Command;

class NumberCollectionCommand extends Command
{
    protected $signature = 'numbers-select';

    protected $description = 'numbers';

    public function handle()
    {
        ini_set('memory_limit', -1);

        $initTime = microtime(true);
        $collection = new NumberCollection();

        // 10000000 * 10 - numbers
        for ($i = 0; $i < 10000000; $i++) {
            $collection->add(rand(-100000, 100000));
            $collection->add(rand(-100000, 100000));
            $collection->add(rand(-100000, 100000));
            $collection->add(rand(-100000, 100000));
            $collection->add(rand(-100000, 100000));
            $collection->add(rand(-100000, 100000));
            $collection->add(rand(-100000, 100000));
            $collection->add(rand(-100000, 100000));
            $collection->add(rand(-100000, 100000));
            $collection->add(rand(-100000, 100000));
        }

        $selectTime = microtime(true);

        $collection->optimize();
        $collection->select(-50000, 50000);

        // ~ 1.1110789775848s
        print_r("Время выполнения скрипта: " . (microtime(true) - $initTime) . PHP_EOL);
        // ~ 0.27299809455872 s
        print_r("Время выбора чисел скрипта: " . (microtime(true) - $selectTime) . PHP_EOL);
    }
}