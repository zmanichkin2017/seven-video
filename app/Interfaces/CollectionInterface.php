<?php

namespace App\Interfaces;

interface CollectionInterface
{
    public function add(int $value): void;

    public function select(int $start, int $end): array;

    public function optimize(): void;


}